package com.example.android.wearable.rzd_android_project.app

import android.app.Application
import com.example.android.wearable.rzd_android_project.di.dataModule
import com.example.android.wearable.rzd_android_project.di.domainModule
import com.example.android.wearable.rzd_android_project.di.retrofitModule
import com.example.android.wearable.rzd_android_project.di.viewModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            val moduleList = listOf(dataModule, domainModule, retrofitModule, viewModule)
            modules(moduleList)
        }
    }
}