package com.example.android.wearable.rzd_android_project.common

import com.example.android.wearable.rzd_android_project.presentation.ViewModel.SplashViewModel


sealed class SocketMessage<T>(val data: T?=null, val message: String?=null) {
    class CommandReceive<T>(data: T?): SocketMessage<T>(data = data)
    class TextReceive<T>(message: String?): SocketMessage<T>(message = message)
}
