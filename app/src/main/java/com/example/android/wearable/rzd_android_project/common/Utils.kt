package com.example.android.wearable.rzd_android_project.common

import android.content.Context
import androidx.appcompat.app.AlertDialog

object Utils {
    fun baseUrl() = "http://10.0.2.2:8000/"
    fun webSocketUrl() = "ws://10.0.2.2:8000/ws/"
    fun optionalAlertDialog(message: String, context: Context) =
        AlertDialog.Builder(context)
        .setMessage(message)
        .create().show()
}