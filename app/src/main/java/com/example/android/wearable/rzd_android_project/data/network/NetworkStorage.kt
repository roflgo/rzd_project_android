package com.example.android.wearable.rzd_android_project.data.network

import com.example.android.wearable.rzd_android_project.common.Resources
import com.example.android.wearable.rzd_android_project.domain.models.AuthModel
import com.example.android.wearable.rzd_android_project.domain.models.ProfileModel
import retrofit2.http.Header

interface NetworkStorage {
    suspend fun authRequest(hashMap: HashMap<String, String>): Resources<AuthModel>
    suspend fun getProfileRequest(token: String): Resources<ProfileModel>
}