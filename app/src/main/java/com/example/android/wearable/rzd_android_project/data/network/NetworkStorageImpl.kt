package com.example.android.wearable.rzd_android_project.data.network

import com.example.android.wearable.rzd_android_project.common.Resources
import com.example.android.wearable.rzd_android_project.domain.models.AuthModel
import com.example.android.wearable.rzd_android_project.domain.models.ErrorModel
import com.example.android.wearable.rzd_android_project.domain.models.ProfileModel
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.lang.Exception


private const val UNEXPECTED_ERROR = "Unexpected Error"

class NetworkStorageImpl(
    private val gson: Gson,
    private val retrofit: RetrofitInterface,
    private val dispatcher: CoroutineDispatcher
): NetworkStorage {
    override suspend fun authRequest(hashMap: HashMap<String, String>): Resources<AuthModel> {
        return withContext(dispatcher) {
            try {
                val result = retrofit.authRequest(hashMap)
                Resources.Success(result)
            } catch (e: HttpException) {
                val gson = gson.fromJson(e.response()?.errorBody()?.string(), ErrorModel::class.java)
                Resources.Error(gson.message)
            } catch (e: Exception) {
                Resources.Error(e.localizedMessage!!.toString())
            }
        }
    }

    override suspend fun getProfileRequest(token: String): Resources<ProfileModel> {
        return withContext(dispatcher) {
            try {
                val result = retrofit.getProfileRequest(token)
                Resources.Success(result)
            } catch (e: HttpException) {
                if (e.code() == 401) {
                    Resources.Error("Unauthorized")
                } else {
                    val gson =
                        gson.fromJson(e.response()?.errorBody()?.string(), ErrorModel::class.java)
                    Resources.Error(gson.message)
                }
            } catch (e: Exception) {
                Resources.Error(e.localizedMessage!!.toString())
            }
        }
    }
}