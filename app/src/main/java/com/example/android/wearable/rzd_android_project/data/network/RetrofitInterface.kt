package com.example.android.wearable.rzd_android_project.data.network

import com.example.android.wearable.rzd_android_project.domain.models.AuthModel
import com.example.android.wearable.rzd_android_project.domain.models.ProfileModel
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface RetrofitInterface {
    @POST("api/auth/")
    suspend fun authRequest(@Body hashMap: HashMap<String, String>): AuthModel

    @GET("api/profile/")
    suspend fun getProfileRequest(@Header("Authorization") token: String): ProfileModel
}