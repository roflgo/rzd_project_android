package com.example.android.wearable.rzd_android_project.data.repository

import com.example.android.wearable.rzd_android_project.common.Resources
import com.example.android.wearable.rzd_android_project.data.network.NetworkStorage
import com.example.android.wearable.rzd_android_project.domain.models.AuthModel
import com.example.android.wearable.rzd_android_project.domain.repository.AuthRepository

class AuthRepositoryImpl (
    private val storage: NetworkStorage): AuthRepository {
    override suspend fun authRepository(hashMap: HashMap<String, String>): Resources<AuthModel> {
        return storage.authRequest(hashMap)
    }
}