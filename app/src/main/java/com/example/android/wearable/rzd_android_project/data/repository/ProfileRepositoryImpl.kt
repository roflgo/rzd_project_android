package com.example.android.wearable.rzd_android_project.data.repository

import com.example.android.wearable.rzd_android_project.common.Resources
import com.example.android.wearable.rzd_android_project.data.network.NetworkStorage
import com.example.android.wearable.rzd_android_project.data.network.RetrofitInterface
import com.example.android.wearable.rzd_android_project.data.storage.SharedPrefClientStorage
import com.example.android.wearable.rzd_android_project.domain.models.ProfileModel
import com.example.android.wearable.rzd_android_project.domain.repository.ProfileRepository
import com.example.android.wearable.rzd_android_project.domain.usecases.GetAccessUseCase

class ProfileRepositoryImpl(private val network: NetworkStorage): ProfileRepository {
    override suspend fun getProfile(token: String): Resources<ProfileModel> {
        return network.getProfileRequest(token)
    }
}