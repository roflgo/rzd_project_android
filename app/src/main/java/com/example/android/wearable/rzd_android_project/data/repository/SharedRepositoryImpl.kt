package com.example.android.wearable.rzd_android_project.data.repository

import com.example.android.wearable.rzd_android_project.data.storage.SharedStorage
import com.example.android.wearable.rzd_android_project.domain.models.ProfileModel
import com.example.android.wearable.rzd_android_project.domain.repository.SharedRepository

class SharedRepositoryImpl(
    private val storage: SharedStorage
): SharedRepository {
    override fun saveToken(token: String) {
        storage.saveToken(token = token)
    }

    override fun getToken(): String {
        return storage.getToken()
    }

    override fun getProfile(): ProfileModel {
        return storage.getProfile()
    }

    override fun saveProfile(profileModel: ProfileModel) {
        return storage.saveProfile(profile = profileModel)
    }
}