package com.example.android.wearable.rzd_android_project.data.storage

import android.content.Context
import android.util.Log
import com.example.android.wearable.rzd_android_project.domain.models.ProfileModel
import com.google.gson.Gson

private const val SHARED_NAME = "RZD_STORAGE"
private const val TOKEN_KEY = "ACCESS_TOKEN"
private const val PROFILE_KEY = "PROFILE"

class SharedPrefClientStorage(context: Context,
val gson: Gson?): SharedStorage {

    private val sharedPreferences = context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE)

    override fun saveToken(token: String) {
        Log.d(TOKEN_KEY, token)
        sharedPreferences.edit().putString(TOKEN_KEY, token).apply()
    }

    override fun getToken(): String {
        Log.d(TOKEN_KEY, sharedPreferences.getString(TOKEN_KEY, "").toString())
        return sharedPreferences.getString(TOKEN_KEY, "").toString()
    }

    override fun saveProfile(profile: ProfileModel) {
        sharedPreferences.edit().putString(PROFILE_KEY, profile.toString()).apply()
    }

    override fun getProfile(): ProfileModel {
        val profile = sharedPreferences.getString(PROFILE_KEY, "").toString()
        return gson!!.fromJson(profile, ProfileModel::class.java)
    }
}