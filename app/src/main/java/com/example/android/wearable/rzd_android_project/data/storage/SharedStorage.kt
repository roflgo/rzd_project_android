package com.example.android.wearable.rzd_android_project.data.storage

import com.example.android.wearable.rzd_android_project.domain.models.ProfileModel

interface SharedStorage {
    fun saveToken(token: String)
    fun getToken(): String
    fun saveProfile(profile: ProfileModel)
    fun getProfile(): ProfileModel
}