package com.example.android.wearable.rzd_android_project.data.websocket

import android.content.Context
import android.util.Log
import com.example.android.wearable.rzd_android_project.common.SocketMessage
import com.example.android.wearable.rzd_android_project.common.Utils
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import okhttp3.*
import okio.ByteString
import java.util.concurrent.TimeUnit

class EchoWebSocketListener(
    private val gson: Gson,
    private val channel: Channel<SocketMessage<ReceiveMessage>>
    ) : WebSocketListener(), SocketRepository {
    private val channelReceive = channel

    override fun onOpen(webSocket: WebSocket, response: Response) {
    }

    override fun onMessage(webSocket: WebSocket, text: String) {
        val resultConvert = gson.fromJson(text, ReceiveMessage::class.java)
        Log.d("Socket", resultConvert.toString())
        runBlocking {
            channelReceive.send(SocketMessage.CommandReceive(resultConvert))
        }
    }

    override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
        Log.d("Socket", "Receiving bytes : " + bytes.hex())
    }

    override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
        webSocket.close(NORMAL_CLOSURE_STATUS, null)
        Log.d("Socket", "Closing : $code / $reason")
    }

    override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
        output("Error : " + t.message)
    }

    companion object {
        private val NORMAL_CLOSURE_STATUS = 1000
    }

    private fun output(txt: String) {
        Log.v("WSS", txt)
    }

    override fun openSocket(token: String): WebSocket {
        val client = OkHttpClient.Builder()
            .readTimeout(3, TimeUnit.SECONDS)
            .build()
        val request = Request.Builder()
            .url(Utils.webSocketUrl() + "?token=$token") // 'wss' - для защищенного канала
            .build()
        val wsListener = EchoWebSocketListener(gson, channel)
        val webSocket = client.newWebSocket(request, wsListener)
        client.dispatcher.executorService.shutdown()
        return webSocket
    }

    override fun closeSocket() {

    }
}