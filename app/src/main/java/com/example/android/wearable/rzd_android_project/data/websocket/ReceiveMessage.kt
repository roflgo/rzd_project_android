package com.example.android.wearable.rzd_android_project.data.websocket

import java.util.stream.Stream

data class ReceiveMessage(val text: String,
val id: String, val created_at_formatted: String)