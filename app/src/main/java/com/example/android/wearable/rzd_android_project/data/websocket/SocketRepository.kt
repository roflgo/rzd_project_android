package com.example.android.wearable.rzd_android_project.data.websocket

import android.content.Context
import okhttp3.WebSocket

interface SocketRepository {
    fun openSocket(token: String): WebSocket
    fun closeSocket()
}