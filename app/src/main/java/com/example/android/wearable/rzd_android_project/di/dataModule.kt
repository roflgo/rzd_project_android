package com.example.android.wearable.rzd_android_project.di

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.example.android.wearable.rzd_android_project.common.SocketMessage
import com.example.android.wearable.rzd_android_project.common.Utils
import com.example.android.wearable.rzd_android_project.data.network.NetworkStorage
import com.example.android.wearable.rzd_android_project.data.network.NetworkStorageImpl
import com.example.android.wearable.rzd_android_project.data.repository.AuthRepositoryImpl
import com.example.android.wearable.rzd_android_project.data.repository.ProfileRepositoryImpl
import com.example.android.wearable.rzd_android_project.data.repository.SharedRepositoryImpl
import com.example.android.wearable.rzd_android_project.data.storage.SharedPrefClientStorage
import com.example.android.wearable.rzd_android_project.data.storage.SharedStorage
import com.example.android.wearable.rzd_android_project.data.websocket.EchoWebSocketListener
import com.example.android.wearable.rzd_android_project.data.websocket.ReceiveMessage
import com.example.android.wearable.rzd_android_project.data.websocket.SocketRepository
import com.example.android.wearable.rzd_android_project.domain.repository.AuthRepository
import com.example.android.wearable.rzd_android_project.domain.repository.ProfileRepository
import com.example.android.wearable.rzd_android_project.domain.repository.SharedRepository
import com.example.android.wearable.rzd_android_project.domain.usecases.GetAccessUseCase
import com.google.gson.Gson
import kotlinx.coroutines.channels.Channel
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.WebSocket
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import java.util.concurrent.TimeUnit

val dataModule = module {
    single<NetworkStorage> {
        NetworkStorageImpl(get(), get(), get())
    }

    single<AuthRepository> {
        AuthRepositoryImpl(get())
    }

    single<SharedStorage> {
        SharedPrefClientStorage(androidContext(), get())
    }

    single<SharedRepository> {
        SharedRepositoryImpl(get())
    }

    single<ProfileRepository> {
        ProfileRepositoryImpl(get())
    }

    fun provideSharedPref(context: Context): SharedPreferences {
        return context.getSharedPreferences("RZD_STORAGE", Context.MODE_PRIVATE)
    }

    single {
        provideSharedPref(androidContext())
    }

    fun provideChannel(): Channel<Any> {
        return Channel(10)
    }

    single {
        provideChannel()
    }



    factory<SocketRepository> {
        EchoWebSocketListener(get(), get())
    }
}