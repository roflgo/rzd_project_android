package com.example.android.wearable.rzd_android_project.di

import com.example.android.wearable.rzd_android_project.domain.repository.ProfileRepository
import com.example.android.wearable.rzd_android_project.domain.usecases.*
import org.koin.dsl.module

val domainModule = module {
    single {
        AuthUseCase(get())
    }

    single {
        SaveAccessUseCase(get())
    }

    single {
        GetAccessUseCase(get())
    }

    single {
        GetProfileUseCase(get())
    }

    single {
        SaveProfileUseCase(get())
    }

    single {
        GetProfileConnectionUseCase(get())
    }
}