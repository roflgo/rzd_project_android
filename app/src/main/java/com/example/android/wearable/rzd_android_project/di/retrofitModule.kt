package com.example.android.wearable.rzd_android_project.di

import android.content.Context
import com.example.android.wearable.rzd_android_project.common.Utils
import com.example.android.wearable.rzd_android_project.data.network.RetrofitInterface
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

val retrofitModule = module {
    fun okHttpClient() : OkHttpClient {
        val logi = HttpLoggingInterceptor()
        logi.setLevel(HttpLoggingInterceptor.Level.BASIC)
        logi.setLevel(HttpLoggingInterceptor.Level.BODY)
        logi.setLevel(HttpLoggingInterceptor.Level.HEADERS)
        val client = OkHttpClient.Builder()
            .addInterceptor(logi)
            .build()
        return client
    }

    fun provideGson(): Gson {
        return Gson()
    }

    fun provideDispatcher(): CoroutineDispatcher {
        return Dispatchers.IO
    }

    fun retrofitObject(client: OkHttpClient): RetrofitInterface {
        return Retrofit.Builder()
            .baseUrl(Utils.baseUrl())
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(RetrofitInterface::class.java)
    }

    factory {
        retrofitObject(get())
    }

    single {
        okHttpClient()
    }

    single {
        provideDispatcher()
    }

    single {
        provideGson()
    }
}