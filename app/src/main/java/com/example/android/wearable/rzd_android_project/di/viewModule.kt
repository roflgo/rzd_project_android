package com.example.android.wearable.rzd_android_project.di

import com.example.android.wearable.rzd_android_project.presentation.ViewModel.LoginViewModel
import com.example.android.wearable.rzd_android_project.presentation.ViewModel.MainViewModel
import com.example.android.wearable.rzd_android_project.presentation.ViewModel.MessageViewModel
import com.example.android.wearable.rzd_android_project.presentation.ViewModel.SplashViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModule = module {
    viewModel {
        LoginViewModel(get(), get())
    }

    viewModel {
        SplashViewModel(get(), get(), get())
    }

    viewModel {
        MainViewModel(get(), get())
    }

    viewModel {
        MessageViewModel(get(), get(), get())
    }
}