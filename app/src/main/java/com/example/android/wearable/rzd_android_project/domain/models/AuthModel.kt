package com.example.android.wearable.rzd_android_project.domain.models

data class AuthModel(val access_token: String, val refresh_token: String, val success: Boolean)