package com.example.android.wearable.rzd_android_project.domain.models

data class ErrorModel(
    val success: Boolean,
    val message: String
)