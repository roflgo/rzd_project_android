package com.example.android.wearable.rzd_android_project.domain.models

class ProfileModel(
    val first_name: String?,
    val middle_name: String?,
    val role: Int?,
    val login: String,
)