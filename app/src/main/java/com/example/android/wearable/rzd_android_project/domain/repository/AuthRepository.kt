package com.example.android.wearable.rzd_android_project.domain.repository

import com.example.android.wearable.rzd_android_project.common.Resources
import com.example.android.wearable.rzd_android_project.domain.models.AuthModel

interface AuthRepository {
    suspend fun authRepository(hashMap: HashMap<String, String>): Resources<AuthModel>
}