package com.example.android.wearable.rzd_android_project.domain.repository

import com.example.android.wearable.rzd_android_project.common.Resources
import com.example.android.wearable.rzd_android_project.domain.models.ProfileModel

interface ProfileRepository {
    suspend fun getProfile(token: String): Resources<ProfileModel>
}