package com.example.android.wearable.rzd_android_project.domain.repository

import com.example.android.wearable.rzd_android_project.domain.models.ProfileModel

interface SharedRepository {
    fun saveToken(token: String)
    fun getToken(): String
    fun getProfile(): ProfileModel
    fun saveProfile(profileModel: ProfileModel)
}