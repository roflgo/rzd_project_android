package com.example.android.wearable.rzd_android_project.domain.usecases

import com.example.android.wearable.rzd_android_project.common.Resources
import com.example.android.wearable.rzd_android_project.data.repository.AuthRepositoryImpl
import com.example.android.wearable.rzd_android_project.domain.models.AuthModel
import com.example.android.wearable.rzd_android_project.domain.repository.AuthRepository

class AuthUseCase(private val repository: AuthRepository) {
    suspend fun execute(hashMap: HashMap<String, String>): Resources<AuthModel> {
        return repository.authRepository(hashMap)
    }
}