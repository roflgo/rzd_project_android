package com.example.android.wearable.rzd_android_project.domain.usecases

import com.example.android.wearable.rzd_android_project.domain.repository.SharedRepository

class GetAccessUseCase(private val storage: SharedRepository) {
    fun execute(): String {
        return storage.getToken()
    }
}