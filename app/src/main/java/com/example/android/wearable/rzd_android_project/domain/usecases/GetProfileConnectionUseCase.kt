package com.example.android.wearable.rzd_android_project.domain.usecases

import com.example.android.wearable.rzd_android_project.common.Resources
import com.example.android.wearable.rzd_android_project.domain.models.ProfileModel
import com.example.android.wearable.rzd_android_project.domain.repository.ProfileRepository

class GetProfileConnectionUseCase(private val storage: ProfileRepository) {
    suspend fun execute(token: String): Resources<ProfileModel> {
        return storage.getProfile(token = token)
    }
}