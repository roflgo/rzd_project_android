package com.example.android.wearable.rzd_android_project.domain.usecases

import com.example.android.wearable.rzd_android_project.domain.models.ProfileModel
import com.example.android.wearable.rzd_android_project.domain.repository.SharedRepository

class GetProfileUseCase(private val storage: SharedRepository) {
    fun execute(): ProfileModel {
        return storage.getProfile()
    }
}