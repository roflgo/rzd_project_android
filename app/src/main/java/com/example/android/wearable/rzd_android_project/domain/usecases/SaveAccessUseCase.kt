package com.example.android.wearable.rzd_android_project.domain.usecases

import com.example.android.wearable.rzd_android_project.domain.repository.SharedRepository

class SaveAccessUseCase(
    private val storage: SharedRepository
) {
    fun execute(token: String) {
        storage.saveToken(token)
    }
}