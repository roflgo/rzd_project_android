package com.example.android.wearable.rzd_android_project.domain.usecases

import com.example.android.wearable.rzd_android_project.domain.models.ProfileModel
import com.example.android.wearable.rzd_android_project.domain.repository.SharedRepository

class SaveProfileUseCase(
    private val storage: SharedRepository
) {
    fun execute(profile: ProfileModel) {
        storage.saveProfile(profileModel = profile)
    }
}