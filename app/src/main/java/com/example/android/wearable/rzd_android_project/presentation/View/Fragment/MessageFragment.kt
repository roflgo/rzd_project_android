package com.example.android.wearable.rzd_android_project.presentation.View.Fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.android.wearable.rzd_android_project.R
import com.example.android.wearable.rzd_android_project.common.SocketMessage
import com.example.android.wearable.rzd_android_project.data.websocket.ReceiveMessage
import com.example.android.wearable.rzd_android_project.databinding.FragmentMessageBinding
import com.example.android.wearable.rzd_android_project.presentation.ViewModel.MessageViewModel
import kotlinx.coroutines.channels.consume
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class MessageFragment : Fragment(R.layout.fragment_message) {
    private val binding: FragmentMessageBinding by viewBinding()
    private val vm: MessageViewModel by viewModel()
    private var activeConnection: Boolean = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObserver()
        initClick()
    }

    private fun initObserver() {
        vm.statusConnectionLiveData.observe(requireActivity()) {
            activeConnection = it
            if (it) {
                binding.startEndWorkBtn.text = "Завершить работу"
            } else {
                binding.startEndWorkBtn.text = "Начать работу"
            }
        }
        lifecycleScope.launch{
            vm.channel.consumeEach {
                when(it) {
                    is SocketMessage.CommandReceive -> {
                        binding.textSocket.text = it.data!!.text
                        binding.viewMessage.isEnabled = true
                        binding.dateTextSocket.text = it.data.created_at_formatted
                    }
                    is SocketMessage.TextReceive -> {

                    }
                }
            }
        }
    }

    private fun initClick() {
        binding.startEndWorkBtn.setOnClickListener{
            if (!activeConnection) {
                vm.startConnection()
            } else {
                vm.closeConnection()
            }
        }
        binding.viewMessage.setOnClickListener{
            binding.textSocket.text = ""
            binding.dateTextSocket.text = ""
            binding.viewMessage.isEnabled = false
        }
    }
}