package com.example.android.wearable.rzd_android_project.presentation.View

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.android.wearable.rzd_android_project.R
import com.example.android.wearable.rzd_android_project.databinding.ActivityLoginBinding
import com.example.android.wearable.rzd_android_project.di.viewModule
import com.example.android.wearable.rzd_android_project.presentation.ViewModel.LoginViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity(R.layout.activity_login) {
    private val binding: ActivityLoginBinding by viewBinding()
    private val viewModel by viewModel<LoginViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBinding()
        initObserver()
    }

    private fun initBinding() {
        binding.buttonLogin.setOnClickListener{
            viewModel.validateData(binding.loginInput.text.toString(),
                binding.passwordInput.text.toString())
        }
    }

    private fun initObserver() {
        viewModel.errorLiveData.observe(this) {
            AlertDialog.Builder(this)
                .setMessage(it)
                .create().show()
        }

        viewModel.errorValidateLiveData.observe(this) {
            AlertDialog.Builder(this)
                .setMessage(it)
                .create().show()
        }

        viewModel.successLiveData.observe(this) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}