package com.example.android.wearable.rzd_android_project.presentation.View

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.android.wearable.rzd_android_project.R
import com.example.android.wearable.rzd_android_project.databinding.ActivityMainBinding
import com.example.android.wearable.rzd_android_project.presentation.View.Fragment.MessageFragment
import com.example.android.wearable.rzd_android_project.presentation.ViewModel.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity(R.layout.activity_main) {
    private val vm: MainViewModel by viewModel()
    private val binding: ActivityMainBinding by viewBinding()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initObserver()
        supportFragmentManager.beginTransaction()
            .replace(binding.fragmentTransaction.id, MessageFragment())
            .commit()
    }

    private fun initObserver() {
        vm.tokenLiveData.observe(this) {

        }
//        lifecycleScope.launch{
//            vm.channels.consumeEach {
//                binding.textChannel.text = it.text
//            }
//        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val count = supportFragmentManager.backStackEntryCount

        if (count == 0) {
            super.onBackPressed()
            //additional code
        } else {
            supportFragmentManager.popBackStack()
        }
    }
}