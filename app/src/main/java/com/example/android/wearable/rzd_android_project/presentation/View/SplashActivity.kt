package com.example.android.wearable.rzd_android_project.presentation.View

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.android.wearable.rzd_android_project.R
import com.example.android.wearable.rzd_android_project.common.Utils
import com.example.android.wearable.rzd_android_project.databinding.ActivitySplashBinding
import com.example.android.wearable.rzd_android_project.presentation.ViewModel.SplashViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

@SuppressLint("CustomSplashScreen")
class SplashActivity : AppCompatActivity(R.layout.activity_splash) {
    private val vm: SplashViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initObserver()
        vm.startTimerLoading()
    }

    private fun initObserver() {
        vm.tokenLiveData.observe(this) { it ->
            if (it == "") {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                vm.getProfile(it)
            }
        }

        vm.profileLiveData.observe(this) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        vm.errorLiveData.observe(this) {
            if (it == "Unauthorized") {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                Utils.optionalAlertDialog(it, this)
            }
        }
    }
}