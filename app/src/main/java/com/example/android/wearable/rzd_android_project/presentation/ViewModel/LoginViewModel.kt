package com.example.android.wearable.rzd_android_project.presentation.ViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.android.wearable.rzd_android_project.common.Resources
import com.example.android.wearable.rzd_android_project.domain.usecases.AuthUseCase
import com.example.android.wearable.rzd_android_project.domain.usecases.SaveAccessUseCase
import kotlinx.coroutines.launch

class LoginViewModel(
    private val authUseCase: AuthUseCase,
    private val saveTokenUseCase: SaveAccessUseCase
): ViewModel() {
    private val errorMutableLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> = errorMutableLiveData

    private val errorValidateMutableData = MutableLiveData<String>()
    val errorValidateLiveData: LiveData<String> = errorValidateMutableData

    private val successAuthLiveData = MutableLiveData<Boolean>()
    val successLiveData: LiveData<Boolean> = successAuthLiveData

    fun validateData(username: String, password: String) {
        if (username.isNotEmpty() && password.isNotEmpty()) {
            val hashMap: HashMap<String, String> = hashMapOf()
            hashMap.put("username", username)
            hashMap.put("password", password)
            viewModelScope.launch {
                authRequest(hashMap)
            }
        } else {
            errorValidateMutableData.postValue("Все поля должны быть заполнены!")
        }
    }

    private suspend fun authRequest(hashMap: HashMap<String, String>) {
        val result = authUseCase.execute(hashMap)
        when(result) {
            is Resources.Error -> {
                errorMutableLiveData.postValue(result.error)
            }
            is Resources.Success -> {
                saveTokenUseCase.execute(result.data!!.access_token)
                successAuthLiveData.postValue(true)
            }
        }
    }
}