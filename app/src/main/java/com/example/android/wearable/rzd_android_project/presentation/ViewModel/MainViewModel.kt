package com.example.android.wearable.rzd_android_project.presentation.ViewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.wearable.rzd_android_project.data.websocket.ReceiveMessage

import com.example.android.wearable.rzd_android_project.domain.usecases.GetAccessUseCase
import kotlinx.coroutines.channels.Channel
import okhttp3.*
import java.lang.Exception

class MainViewModel(
    private val getAccessUseCase: GetAccessUseCase,
    private val channel: Channel<ReceiveMessage>
): ViewModel() {
    private val tokenMutableLiveData = MutableLiveData<String>()
    val tokenLiveData: LiveData<String> = tokenMutableLiveData
}