package com.example.android.wearable.rzd_android_project.presentation.ViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.wearable.rzd_android_project.common.SocketMessage
import com.example.android.wearable.rzd_android_project.data.websocket.EchoWebSocketListener
import com.example.android.wearable.rzd_android_project.data.websocket.ReceiveMessage
import com.example.android.wearable.rzd_android_project.data.websocket.SocketRepository
import com.example.android.wearable.rzd_android_project.domain.usecases.GetAccessUseCase
import kotlinx.coroutines.channels.Channel
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.WebSocket

class MessageViewModel(
    private val socket: SocketRepository,
    private val channelSocket: Channel<SocketMessage<ReceiveMessage>>,
    private val getAccessUseCase: GetAccessUseCase
): ViewModel() {
    private var socketObject: WebSocket? = null
    private val statusConnectionMutableLiveData = MutableLiveData<Boolean>()
    val statusConnectionLiveData: LiveData<Boolean> = statusConnectionMutableLiveData

    val channel = channelSocket

    fun startConnection() {
        socketObject = socket.openSocket(getAccessUseCase.execute())
        statusConnectionMutableLiveData.value = true
    }

    fun closeConnection() {
        socketObject!!.close(1000, null)
        statusConnectionMutableLiveData.value = false
    }
}