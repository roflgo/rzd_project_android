package com.example.android.wearable.rzd_android_project.presentation.ViewModel

import android.os.CountDownTimer
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.android.wearable.rzd_android_project.common.Resources
import com.example.android.wearable.rzd_android_project.domain.models.ProfileModel
import com.example.android.wearable.rzd_android_project.domain.usecases.GetAccessUseCase
import com.example.android.wearable.rzd_android_project.domain.usecases.GetProfileConnectionUseCase
import com.example.android.wearable.rzd_android_project.domain.usecases.GetProfileUseCase
import com.example.android.wearable.rzd_android_project.domain.usecases.SaveProfileUseCase
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashViewModel(
    private val getProfileConnectionUseCase: GetProfileConnectionUseCase,
    private val getAccessUseCase: GetAccessUseCase,
    private val saveProfileUseCase: SaveProfileUseCase
): ViewModel() {
    private val tokenMutableLiveData = MutableLiveData<String>()
    val tokenLiveData: LiveData<String> = tokenMutableLiveData

    private val errorMutableLiveData = MutableLiveData<String>()
    val errorLiveData: LiveData<String> = errorMutableLiveData

    private val profileMutableLiveData = MutableLiveData<ProfileModel>()
    val profileLiveData: LiveData<ProfileModel> = profileMutableLiveData

    fun getProfile(token: String) {
        viewModelScope.launch {
            val resultProfileRequest = getProfileConnectionUseCase.execute(token = "Bearer $token")
            when(resultProfileRequest) {
                is Resources.Error -> {
                    errorMutableLiveData.value = resultProfileRequest.error
                }
                is Resources.Success -> {
                    saveProfileUseCase.execute(resultProfileRequest.data!!)
                    profileMutableLiveData.value = resultProfileRequest.data
                }
            }
        }
    }

    private fun getToken() {
        tokenMutableLiveData.value = getAccessUseCase.execute()
    }

    fun startTimerLoading() {
        val timer = object : CountDownTimer(100, 2000) {
            override fun onTick(p0: Long) {

            }

            override fun onFinish() {
                getToken()
            }
        }
        timer.start()
    }
}